import os.path as osp
import numpy as np
import pickle
import logging
from collections import defaultdict

class PairGenerator():
  def __init__(self, X, Y, same_prob, rng, shuffle=True):
    self.X = X
    self.Y = Y
    self.same_prob = same_prob
    self.rng = rng
    self.shuffle = shuffle
    self.class_indices = defaultdict(list)
    for i, y in enumerate(self.Y):
      self.class_indices[y].append(i)
    self.classes = list(self.class_indices.keys())

  def size(self):
    return self.X.shape[0]

  def sample_from_class(self, y):
    idx = self.rng.choice(self.class_indices[y])
    assert(self.Y[idx] == y)
    return idx

  def get_data(self):
    indices = np.arange(self.size())
    if self.shuffle:
      self.rng.shuffle(indices)
    for i in indices:
      y = self.Y[i]
      same_class = self.rng.binomial(1, self.same_prob)
      if same_class == 1:
        y_out = y
        idx = self.sample_from_class(y)
        x2 = self.X[idx][None]
      else:
        y_out = self.rng.choice(self.classes)
        idx = self.sample_from_class(y_out)
        x2 = self.X[idx][None]
        y_out = int(y_out) if y_out == y else 0
      yield self.X[i][None], x2, y_out

