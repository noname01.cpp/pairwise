import tensorflow as tf
from dataflow.pair_generator import PairGenerator
import json
import logging
from easydict import EasyDict
from dataflow.utils import fix_rng_seed
from model.util import batched_gather
import os
import pickle
import numpy as np

def get_lr_schedule(lr_schedule):
  boundaries = lr_schedule.boundaries
  values = lr_schedule.values
  return tf.keras.optimizers.schedules.PiecewiseConstantDecay(boundaries, values)

def corloc(top_subproblem, labels, corloc_list):
  ntotal = tf.cast(tf.shape(top_subproblem)[-1], tf.float32)
  for i in range(tf.shape(labels)[0]):
    res = batched_gather(top_subproblem[i, ..., tf.newaxis],
                         labels[i])
    corloc_list.append(tf.reduce_sum(res)/ntotal)

def get_best_acc(dir_path):
  top_pkl_file = os.path.join(dir_path, 'top.pkl')
  if os.path.isfile(top_pkl_file):
    with open(top_pkl_file, 'rb') as f:
      top = pickle.load(f)
    return top['best']
  return 0.0

def save_best_acc(dir_path, best_acc, iteration):
  top_pkl_file = os.path.join(dir_path, 'top.pkl')
  top = {'best': best_acc, 'iteration': iteration}
  with open(top_pkl_file, 'wb') as f:
    pickle.dump(top, f)

def read_py2_pickle(name):
  logging.info(' - Reading pickle {}'.format(name))
  with open(os.path.join('data', 'ImageSet', name+'.pkl'), 'rb') as f:
    return pickle.load(f, encoding='latin1')

def l2_normalize(fea):
  return fea / (1e-12 + np.linalg.norm(fea, axis=-1)[...,np.newaxis])

def load_data(config):
  rng = np.random.RandomState(7)
  logging.info("Loading datasets...")
  ds = read_py2_pickle(config.dataset.dataset_name)
  feas_dict = read_py2_pickle(config.dataset.feas_name)
  #filter multiple object images
  feas = []
  labels = []
  for i, roi in enumerate(ds['rois']):
    if len(np.unique(roi['classes'])) == 1:
      cls = roi['classes'][0]
      labels.append(cls)
      feas.append(feas_dict[ds['images'][i]])
  logging.info("Found {} single object images.".format(len(labels)))
  feas = np.vstack(feas)
  labels = np.hstack(labels)
  indices = np.arange(len(labels))
  rng.shuffle(indices)
  feas = feas[indices]
  labels = labels[indices]
  validation_num = int(len(labels)*config.dataset.validation_ratio)
  if validation_num > 0:
    val_feas, val_labels = feas[:validation_num], labels[:validation_num]
  else: #NOTE if validation_num is 0, we use training as val
    logging.info("Using training data as validation")
    validation_num = 0
    val_feas = feas.copy()
    val_labels = labels.copy()

  train_feas, train_labels = feas[validation_num:], labels[validation_num:]
  if config.dataset.l2_normalize:
    train_feas = l2_normalize(train_feas)
    val_feas = l2_normalize(val_feas)
  return train_feas, train_labels, val_feas, val_labels

def get_dataset(config, X, Y, training):
  rng = np.random.RandomState(config.seed)
  same_prob = config.dataset.same_class_prob
  if training:
    gen = PairGenerator(X, Y, same_prob, rng, shuffle=True)
  else:
    gen = PairGenerator(X, Y, same_prob, rng, shuffle=False)
  dataset = tf.data.Dataset.from_generator(gen.get_data,
                                            (tf.float32, tf.float32, tf.int32)).prefetch(
                                                 config.train.prefetch_buffer_size)
  if training:
    return dataset.batch(config.train.batch_size).repeat()
  else:
    return dataset.batch(1)

def get_config(path):
  with open(path,'r') as f:
    return EasyDict(json.load(f))

def set_logger(log_path):
  """Set the logger to log info in terminal and file `log_path`.

  In general, it is useful to have a logger so that every output to the terminal is saved
  in a permanent file. Here we save it to `model_dir/train.log`.

  Example:
  ```
  logging.info("Starting training...")
  ```

  Args:
      log_path: (string) where to log
  """
  logger = logging.getLogger()
  logger.setLevel(logging.INFO)

  if not logger.handlers:
    # Logging to a file
    file_handler = logging.FileHandler(log_path)
    file_handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s: %(message)s'))
    logger.addHandler(file_handler)

    # Logging to console
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(logging.Formatter('%(message)s'))
    logger.addHandler(stream_handler)
