import tensorflow as tf
from tensorflow import keras

class RelationModule(keras.layers.Layer):
  '''Relation module class. It takes two 3D tensors with the same dimension and
      produces the relation scores between all pairwise elements of the input tensors.
  '''
  def __init__(self, config):
    '''
    Args:
      config: config object
    '''
    super(RelationModule, self).__init__()
    self.config = config
    self.D = config.model.pre_fc.dim
    self._regularization_scale = self.config.model.regularization_scale

  def build(self, input_shapes):
    '''
    input_shapes: list of two tensors shapes of N,1,fea_dim and N,1,fea_dim
    '''
    assert (isinstance(input_shapes, list) and len(input_shapes)==2
                      ),'input_shapes must be a list of two tensors'
    assert (input_shapes[0][-1] == input_shapes[1][-1]
              ),'two tensors should have the same feature dimensions'

    self.fea_dim = input_shapes[0][-1]
    self.pre_fc = keras.layers.Dense(self.D, input_dim=self.fea_dim,
                                     activation=None,
                                     use_bias=True,
                                     kernel_initializer='glorot_uniform',
                                     kernel_regularizer=keras.regularizers.l2(
                                       self._regularization_scale))
    self.pre_fc.build(input_shape=[None, None,self.fea_dim])
    self.tanh_fc = keras.layers.Dense(self.D, input_dim=self.D*2,
        activation=None,
        use_bias=False,
        kernel_initializer='glorot_uniform',
        kernel_regularizer=keras.regularizers.l2(self._regularization_scale))
    self.tanh_fc.build(input_shape=[None,None,None,self.D*2])
    self.sigm_fc = keras.layers.Dense(self.D, input_dim=self.D*2,
        activation=None,
        use_bias=False,
        kernel_initializer='glorot_uniform',
        kernel_regularizer=keras.regularizers.l2(self._regularization_scale))
    self.sigm_fc.build(input_shape=[None,None,None,self.D*2])
    num_out = 1
    if self.config.model.multiclass:
      num_out = self.config.num_classes + 1
    self.score_fc = keras.layers.Dense(num_out, input_dim=self.D,
        activation=None,
        kernel_initializer='glorot_uniform',
        kernel_regularizer=keras.regularizers.l2(self._regularization_scale))
    self.score_fc.build(input_shape=[None,None,None,self.D])
    self.pre_bn = keras.layers.BatchNormalization(epsilon=1e-5)
    self.pre_bn.build(input_shape=[None,None,self.D])
    self.tanh_bn = keras.layers.BatchNormalization(epsilon=1e-5)
    self.tanh_bn.build(input_shape=[None,None,None,self.D])
    self.sigm_bn = keras.layers.BatchNormalization(epsilon=1e-5)
    self.sigm_bn.build(input_shape=[None,None,None,self.D])
    super(RelationModule, self).build(input_shapes)

  #@tf.function
  def call(self, inputs, training=None, tile_second_fea=True):
    assert(isinstance(inputs, list) and len(inputs) == 2
                ),'inputs must be a list of two tensors'
    #fea0 is N,1,fea_dim and fea1 is N,1,fea_dim
    fea0, fea1 = inputs
    ## apply pre_fc
    # concat: fea01: 2N,1,fea_dim
    fea01 = tf.concat([fea0,fea1], axis=0)
    # 2N,1,D
    fea01 = self.pre_fc(fea01)
    fea01 = self.pre_bn(fea01)
    fea01 = tf.nn.relu(fea01)
    # N,1,D 
    fea0, fea1 = tf.split(fea01, 2, axis=0)
    m = tf.shape(fea0)[1]
    #N,M,L,D
    if tile_second_fea:
      fea1 = tf.tile(fea1[:,tf.newaxis],[1,m,1,1])
    l = tf.shape(fea1)[2]
    fea0 = tf.tile(fea0[:,:,tf.newaxis], [1,1,l,1])

    #N,M,L,2D
    fea01 = tf.concat((fea0, fea1), axis=-1)
    #N,M,L,D
    out_tanh = self.tanh_fc(fea01)
    #print("TANH_BN");from IPython import embed;embed()
    #TODO in the paper's code have bn and then tanh
    out_tanh = self.tanh_bn(out_tanh, training)

    out_sigm = self.sigm_fc(fea01)
    out_sigm = self.sigm_bn(out_sigm, training)

    ## WE FOUND OUT RELU ACTIVATIONS
    ## WERE USED IN OUR ORIGINAL CODE
    ## BY MISTAKE. THIS PART IS NECESSARY
    ## FOR REPRODUCING THE RESULTS IN THE
    ## PAPER. IT IS OK TO REMOVE THIS IF
    ## YOU WANT TO TRAIN YOUR OWN MODEL.
    if True:
      out_tanh = tf.nn.relu(out_tanh)
      out_sigm = tf.nn.relu(out_sigm)
    ##

    out_tanh = tf.tanh(out_tanh)
    out_sigm = tf.sigmoid(out_sigm)
    #N,M,L,D
    C = out_tanh*out_sigm + (fea0+fea1)/2
    #N,M,L,1
    scores = self.score_fc(C)
    return scores


