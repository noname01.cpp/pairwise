import tensorflow as tf
import utils
from model.relation_module import RelationModule
import os.path as osp
import logging
import argparse
from IPython import embed
import time
import numpy as np
import os

parser = argparse.ArgumentParser()
parser.add_argument('--experiments_dir', required=True,
          help="Directory containing the experiments")
parser.add_argument('--model', #required=True,
          help="Directory containing the experiments", default='best')

def eval(config, dataset):
  logging.info('Running evaluation for {} iterations'.format(config.eval.iters))
  model = RelationModule(config)
  checkpoint = config.checkpoint
  if osp.isdir(config.checkpoint):
    checkpoint = tf.train.latest_checkpoint(config.checkpoint)

  logging.info('Restoring parameters from {}'.format(checkpoint))
  ckpt = tf.train.Checkpoint(net=model)
  ckpt.restore(checkpoint).expect_partial()
  success_list = []
  inference_time_list = []
  all_preds = []
  all_Ys = []
  for step, dt in enumerate(dataset):
    X1,X2,Y = dt
    start_time = time.time()
    logits = model([X1,X2], training=False)
    inference_time_list.append(time.time() - start_time)
    #NOTE: ASSUMES BATCH==1 in eval
    pred = tf.argmax(logits, axis=-1)[0,0,0].numpy()
    label = Y[0].numpy()

    success = pred == label
    success_list.append(success)
    if (step+1) % config.eval.print_freq == 0:
      logging.info('step {}/{} ({:0.3f} sec/iter)'.format(step+1, config.eval.iters,
                            np.mean(inference_time_list[-config.eval.print_freq:])))
    all_preds.append(pred)
    all_Ys.append(label)

    if step > config.eval.iters:
      break

  all_preds = np.array(all_preds)
  all_Ys = np.array(all_Ys)
  same = all_preds[all_Ys != 0] == all_Ys[all_Ys != 0]
  same_acc = np.mean(same)
  same_std = np.std(same)
  mean_acc = np.mean(success_list)
  std_acc = np.std(success_list)
  print('- done.')
  logging.info('Accuracy is {} +- {}'.format(mean_acc, std_acc))
  logging.info('SAME Accuracy is {} +- {}'.format(same_acc, same_std))
  return mean_acc, std_acc, success_list

def main():
  #tf.config.gpu.set_per_process_memory_growth(True)
  # Load the config from json file
  args = parser.parse_args()
  json_path = osp.join(args.experiments_dir, 'config.json')
  assert osp.isfile(json_path), "No json configuration file found at {}".format(json_path)
  config = utils.get_config(json_path)
  config.checkpoint = os.path.join(args.experiments_dir, args.model)
  utils.set_logger(osp.join(args.experiments_dir, 'test.log'))
  logging.info('Loading the dataset...')
  train_X, train_Y, val_X, val_Y = utils.load_data(config)
  test_dataset = utils.get_dataset(config, val_X, val_Y, training=False)
  logging.info('- done.')
  eval(config, test_dataset)

if __name__ == '__main__':
  main()

